Для того щоб додати нове домашнє завдання в новий репозиторій потрібно:
1. Створити новий репозиторій та при створенні обрати «public» аби ментом зміг зайти та перевірити домашнє завдання.
2. Клонувати репозиторій у «vscode».
3. Перевірити чи всі потрібні нам файли відслідковуються а якщо ні то добавити їх за допомогою команди у терміналі «git add .» для того щоб були добавлені всі файли або ж «git add …» (замість «…» повинен бути вказаний шлях до конкретної папки)
4. Зробити «git commit -m “…"» щоб підготувати усі файли до відправки на репозиторій.
5. Прописати «git push» для того щоб відправити всі файли.